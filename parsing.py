# from xml.etree.ElementTree import parse
import xmltodict
import json
def parse_data(filepath):
    f = open(filepath, 'r', encoding='UTF8')
    data = f.read()
    f.close()
    # xml 데이터 파싱
    data = xmltodict.parse(data)
    json_type = json.dumps(data)
    data = json.loads(json_type)
    lst = []
    for i in data['USERDATA']['USER']:
        lst.append(i['type'])
    lst = set(lst)
    dis = {

    }
    agg = {

    }
    for i in lst:
        dis[i] = []
        agg[i] = 0
    
    for i in data['USERDATA']['USER']: 
        dis[i['type']].append(i)
        agg[i['type']] += int(i['amount'])

    pro_data = {
        'coins' : data['USERDATA']['COINS'],
        'dis' : dis,
        'agg' : agg
    }
    return pro_data



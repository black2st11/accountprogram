import tkinter
from tkinter import filedialog
from accounts import Account
import tkinter.messagebox
def click(event):
    filename = filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("xml","*.xml"),("all files","*.*")))
    if filename:
        try:    
            Account(filename)
            tkinter.messagebox.showinfo("성공",message='파일 생성에 성공하였습니다.',)
        except :
            tkinter.messagebox.showinfo("실패",message='파일 생성에 실패하였습니다.',)

window = tkinter.Tk()
window.resizable(False, False)
window.title('')
window.iconphoto(False, tkinter.PhotoImage(file='./KakaoTalk_20200611_201238516.png'))
window.geometry("200x200")
btn = tkinter.Button(window, text='파일을 선택해주세요',width=20,height=300)
btn.bind("<Button-1>", click)
btn.pack(pady=70)
window.mainloop()
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from parsing import parse_data
from openpyxl.styles import Font, colors, Border, Fill, Side, PatternFill, Alignment
from openpyxl.worksheet.properties import WorksheetProperties, PageSetupProperties
import time
import datetime
import sys, os

def Account(filepath, savepath):
    def StyleMod(a, b):
        for i in a:
            for j in i:
                if 'fill' in b:
                    j.fill = bg_fill
                if 'border' in b:
                    j.border = default_border
                if 'font' in b:
                    j.font = sub_font
                if 'align' in b:
                    j.alignment = Alignment(horizontal = 'center', vertical= 'center')

    def CellSum(a):
        result = 0
        for i in a:
            for j in i:
                result += int(j.value) 
        return result

    def MakeTitle(col, cnt):
        ws1.merge_cells(chr(col)+str(cnt)+':'+chr(col+1)+str(cnt))
        ws1[chr(col)+str(cnt)]= i
        ws1[chr(col)+str(cnt)].font = sub_font
        ws1[chr(col)+str(cnt)].fill = bg_fill
        ws1[chr(col)+str(cnt)].alignment =default_align
        cnt+=1
        ws1[chr(col)+str(cnt)]='성명'
        ws1[chr(col)+str(cnt)].font = sub_font
        ws1[chr(col)+str(cnt)].fill = bg_fill
        ws1[chr(col)+str(cnt)].alignment =default_align
        ws1[chr(col+1)+str(cnt)]='금액'
        ws1[chr(col+1)+str(cnt)].font = sub_font
        ws1[chr(col+1)+str(cnt)].fill = bg_fill
        ws1[chr(col+1)+str(cnt)].alignment =default_align
        StyleMod(ws1[chr(col)+str(cnt-1)+':'+chr(col+1)+str(cnt)], ['fill'])

    
    week_cnt = datetime.datetime.now().isocalendar()[1]
    #fill c5c5c5
    #style 
    title_font = Font(size=16, bold=True, name='Georgia')
    day_font = Font(size=12, bold=True, name='Georgia')
    sub_title_font = Font(size=11, bold=True, name='Georgia')
    sub_font = Font(size=11, bold=True, name='Georgia')

    bg_fill = PatternFill('solid', fgColor='c5c5c5')
    default_side = Side(color='000000',border_style='thin')
    default_border = Border(right=default_side, left=default_side, top=default_side, bottom=default_side)
    default_align = Alignment(horizontal = 'center', vertical= 'center')
    data = parse_data(filepath)

    wb = Workbook()

    dest_filename = time.strftime('%Y%m%d', time.localtime(time.time()))+'.xlsx'

    ws1 = wb.active
    ws1.title = str(week_cnt)+'주차 헌금입금전표'

    #merge define
    ws1.merge_cells('A1:D1')
    ws1.merge_cells('A2:D2')
    ws1.merge_cells('A3:D3')
    ws1.merge_cells('F3:H3')
    ws1.merge_cells('A12:D12')
    temp_sum = 0

    for i in data['agg']:
        temp_sum += data['agg'][i]
    # 타이틀 부분
    ws1['A1'] = str(week_cnt)+'주차 헌금입금전표'
    ws1['A1'].font = title_font
    ws1.row_dimensions[1].height = 40
    ws1['A2'] = time.strftime('%Y년 %m월 %d일', time.localtime(time.time()))
    ws1['A2'].font = day_font
    ws1.row_dimensions[2].height = 30

    # 항목별 헌금 현황
    ws1['A3'] = '■ 항목별 헌금 현황'
    ws1['A3'].font = sub_title_font
    ws1['A4'] = '헌금항목명'
    ws1['B4'] = '금액'
    ws1['C4'] = '헌금항목명'
    ws1['D4'] = '금액'
    
    col = 65
    row_cnt = 5
    for i in data['agg']:
        ws1[chr(col)+str(row_cnt)] = i
        ws1[chr(col)+str(row_cnt)].alignment = default_align
        ws1[chr(col+1)+str(row_cnt)] = data['agg'][i]
        ws1[chr(col+1)+str(row_cnt)].number_format = '#,##0_);(#,##0)'
        if col == 65:
            col = 67
        else :
            col = 65
            row_cnt+=1
    else:
        if col == 67:
            row_cnt+=1
        ws1.merge_cells('A'+str(row_cnt)+':'+'C'+str(row_cnt))
        ws1['A'+str(row_cnt)] = '헌금 총 합계'
        ws1['A'+str(row_cnt)].alignment = default_align
        ws1['D'+str(row_cnt)] = temp_sum
        ws1['D'+str(row_cnt)].number_format='#,##0_);(#,##0)'
    StyleMod(ws1['A4:D4'],['fill'])
    StyleMod(ws1['A'+str(row_cnt)+':'+'D'+str(row_cnt)],['fill'])
    StyleMod(ws1['A4:D'+str(row_cnt)],['border', 'font'])



    # 금액별 현황
    ws1['F3'] = '■ 헌금 금액별 현황'
    ws1['F3'].font = sub_title_font
    ws1['F4'] = '금액 현황'
    ws1['G4'] = '수량'
    ws1['H4'] = '금액'

    amount = ['오만원권', '일만원권', '오천원권', '일천원권', '오백원', '일백원']
    coins = ['ohManwon', 'manwon', 'ohChoenwon', 'choenwon', 'ohBaekwon', 'baekwon'] 
    mul = [50000, 10000, 5000, 1000, 500, 100]
    for i in range(6):
        ws1['F'+str(5+i)] = amount[i]
        ws1['F'+str(5+i)].alignment = default_align
        ws1['G'+str(5+i)] = int(data['coins'][coins[i]])
        ws1['G'+str(5+i)].number_format='#,##0_);(#,##0)'
        ws1['H'+str(5+i)] = int(data['coins'][coins[i]]) * mul[i]
        ws1['H'+str(5+i)].number_format='#,##0_);(#,##0)'

    StyleMod(ws1['F4:H4'],['fill'])
    StyleMod(ws1['F11:H11'],['fill'])
    StyleMod(ws1['F4:H11'],['border', 'font'])


    ws1['F11'] = '집계'
    ws1['F11'].alignment = default_align
    ws1['G11'] = CellSum(ws1['G5:G10'])
    ws1['G11'].number_format='#,##0_);(#,##0)'
    ws1['H11'] = CellSum(ws1['H5:H10'])
    ws1['H11'].number_format='#,##0_);(#,##0)'



    cnt = 13
    # 개인별 헌금현황
    ws1['A12'] = '■ 개인별 헌금현황'
    ws1['A12'].font = sub_title_font
    col_1 = 65
    for i in data['dis']:
        if i not in ['주정헌금']:
            MakeTitle(col_1, cnt)
            cnt+=2
            if cnt >=79:
                StyleMod(ws1[chr(col_1)+str(13)+':'+chr(col_1+1)+str(cnt-1)], ['border','font'])
                cnt = 13
                col_1 += 2
                MakeTitle(col_1, cnt)
                cnt+=2

            for j in data['dis'][i]:
                ws1[chr(col_1)+str(cnt)]=j['name']
                ws1[chr(col_1)+str(cnt)].font = sub_font
                ws1[chr(col_1+1)+str(cnt)]=int(j['amount'])
                ws1[chr(col_1+1)+str(cnt)].font = sub_font
                ws1[chr(col_1+1)+str(cnt)].number_format = '#,##0_);(#,##0)'
                cnt+=1
                if cnt >=79:
                    StyleMod(ws1[chr(col_1)+str(13)+':'+chr(col_1+1)+str(cnt-1)], ['border','font'])
                    cnt = 13
                    col_1 += 2
                    MakeTitle(col_1, cnt)
                    cnt+=2
            else :
                ws1[chr(col_1)+str(cnt)] = '합계'
                ws1[chr(col_1+1)+str(cnt)] = data['agg'][i]
                ws1[chr(col_1+1)+str(cnt)].number_format = '#,##0_);(#,##0)'
                StyleMod(ws1[chr(col_1)+str(cnt)+':'+chr(col_1+1)+str(cnt)], ['fill','font'])
                cnt +=1
                if cnt >=79:
                    StyleMod(ws1[chr(col_1)+str(13)+':'+chr(col_1+1)+str(cnt-1)], ['border','font'])
                    cnt = 13
                    col_1 += 2
                    MakeTitle(col_1, cnt)
                    cnt+=2
    else:
        StyleMod(ws1[chr(col_1)+str(13)+':'+chr(col_1+1)+str(cnt-1)], ['border','font'])


    # 결재 라인
    ws1.merge_cells('J3:J8')
    ws1.merge_cells('K3:L3')
    ws1.merge_cells('K4:L8')
    ws1.merge_cells('M3:N3')
    ws1.merge_cells('M4:N8')
    ws1.merge_cells('O3:P3')
    ws1.merge_cells('O4:P8')
    ws1['J3'] = '결재'
    ws1['J3'].fill = bg_fill
    for i in range(2,11,2):
        ws1.column_dimensions[get_column_letter(i)].width = 13
    # ws1.row_dimensions[4].height = 50
    # ws1.row_dimensions[5].height = 30
    # ws1.row_dimensions[14].height = 30
    ws1['K3'] = '재정담당'
    ws1['M3'] = '재정부장'
    ws1['O3'] = '당회장'
    StyleMod(ws1['J3:P8'],['border', 'font'])
    StyleMod(ws1['J3:P3'],['fill'])
    StyleMod(ws1['A3:P3'],['align'])

    ws1.page_setup.scale = 64
    ws1.page_setup.paperSize = 9
    ws1.page_margins.left=0.25
    ws1.page_margins.right=0.25
    ws1.page_margins.top = 0.25
    ws1.page_margins.bottom = 0.25
    wb.save(savepath+'\\'+dest_filename)

Account(sys.argv[1],sys.argv[2])